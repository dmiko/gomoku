# Gomoku game

Simple gomoku game, created as a university project.

Don't blame me for Python code quality, I'm a Java developer.

## How to start?
clone repo and run from bash:
```
python3 main.py
```

## Controls
* spacebar - reset game
* left mouse click - set a pawn
