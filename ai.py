from settings import AI_PLAYER
from settings import HUMAN_PLAYER
import copy


class Ai:

    def __init__(self, board):
        self.board = board

    def do_move(self):
        moves = self.board.get_possible_moves()
        if len(moves) == 0:
            self.board.set_cell_value(7, 7, 2)
        else:
            move = self.calculate_best_move()
            self.board.set_cell_value(move[1], move[2], AI_PLAYER)

    def calculate_best_move(self):
        best_offensive_move = self.calculate_next_move(AI_PLAYER)
        best_defensive_move = self.calculate_next_move(HUMAN_PLAYER)

        if best_offensive_move[0] + 1 > best_defensive_move[0]:
            return best_offensive_move
        else:
            return best_defensive_move

    def calculate_next_move(self, player):
        best_move = [-1] * 3
        moves = self.board.get_possible_moves()
        for move in moves:
            virtual_board = copy.deepcopy(self.board)
            virtual_board.set_cell_value(move[0], move[1], player)
            score = virtual_board.get_score(player)
            if score > best_move[0]:
                best_move[0] = score
                best_move[1] = move[0]
                best_move[2] = move[1]
        return best_move
