import pygame
from settings import WIN_SCORE


class Board:

    def __init__(self):
        self.grid = [[0 for x in range(15)] for y in range(15)]

    def draw(self, screen, screen_size, color):
        for (x) in range(screen_size - 25, 0, -50):
            pygame.draw.line(screen, color, (0, x), (screen_size, x))
            pygame.draw.line(screen, color, (x, 0), (x, screen_size))

    def get_cell_value(self, y, x):
        return self.grid[y][x]

    def set_cell_value(self, y, x, value):
        if self.grid[y][x] == 0:
            self.grid[y][x] = value
            return True
        print("That field is already set")
        return False

    def check_horizontal(self, player_number):
        line_score = 0
        total_score = 0
        for y in range(15):
            for x in range(15):
                if self.grid[y][x] == player_number:
                    line_score += 1
                elif line_score == 5:
                    return WIN_SCORE
                elif self.grid[y][x] == 0:
                    if 0 <= line_score < 5:
                        total_score = max(line_score * 5, total_score)
                    line_score = 0
                elif self.grid[y][x] != 0:
                    if 0 <= line_score < 5:
                        total_score = max(line_score, total_score)
                    line_score = 0
        return total_score

    def check_vertically(self, player_number):
        line_score = 0
        total_score = 0
        for x in range(15):
            for y in range(15):
                if self.grid[y][x] == player_number:
                    line_score += 1
                elif line_score == 5:
                    return WIN_SCORE
                elif self.grid[y][x] == 0:
                    if 0 <= line_score < 5:
                        total_score = max(line_score * 5, total_score)
                    line_score = 0
                elif self.grid[y][x] != 0:
                    if 0 <= line_score < 5:
                        total_score = max(line_score, total_score)
                    line_score = 0
        return total_score

    def check_diagonal(self, player_number):
        line_score = 0
        total_score = 0
        for k in range(0, 29):
            start = max(0, k - 15 + 1)
            end = min(14, k)
            for x in range(start, end + 1):
                y = k - x
                if self.grid[y][x] == player_number:
                    line_score += 1
                elif line_score == 5:
                    return WIN_SCORE
                elif self.grid[y][x] == 0:
                    if 0 <= line_score < 5:
                        total_score = max(line_score * 5, total_score)
                    line_score = 0
                elif self.grid[y][x] != 0:
                    if 0 <= line_score < 5:
                        total_score = max(line_score, total_score)
                    line_score = 0

        for k in range(-14, 14):
            start = max(0, k)
            end = min(14 + k - 1, 14)
            for x in range(start, end + 1):
                y = x - k
                if self.grid[y][x] == player_number:
                    line_score += 1
                elif line_score == 5:
                    return WIN_SCORE
                elif self.grid[y][x] == 0:
                    if 0 <= line_score < 5:
                        total_score = max(line_score * 5, total_score)
                    line_score = 0
                elif self.grid[y][x] != 0:
                    if 0 <= line_score < 5:
                        total_score = max(line_score, total_score)
                    line_score = 0
        return total_score

    def get_score(self, player_number):
        return max(self.check_vertically(player_number), self.check_horizontal(player_number),
                   self.check_diagonal(player_number))

    def reset_grid(self):
        self.grid = [[0 for x in range(15)] for y in range(15)]

    def get_possible_moves(self):
        moves = []
        for x in range(15):
            for y in range(15):
                if self.grid[y][x] != 0:
                    continue
                else:
                    if 0 < x < 14 and 0 < y < 14:
                        if self.grid[y + 1][x] != 0 or self.grid[y - 1][x] != 0 or self.grid[y][x + 1] != 0 \
                                or self.grid[y][x - 1] != 0 or self.grid[y + 1][x + 1] != 0 \
                                or self.grid[y - 1][x + 1] != 0 or self.grid[y + 1][x - 1] != 0 \
                                or self.grid[y - 1][x - 1] != 0:
                            moves.append([y, x])
                            continue
                    if 14 > y > 0 == x:
                        if self.grid[y + 1][x] != 0 or self.grid[y - 1][x] != 0 or self.grid[y][x + 1] != 0 \
                                or self.grid[y + 1][x + 1] != 0 or self.grid[y - 1][x + 1] != 0:
                            moves.append([y, x])
                            continue
                    if 14 > x > 0 == y:
                        if self.grid[y + 1][x] != 0 or self.grid[y][x + 1] != 0 \
                                or self.grid[y][x - 1] != 0 or self.grid[y + 1][x + 1] != 0 \
                                or self.grid[y + 1][x - 1] != 0:
                            moves.append([y, x])
                            continue
                    if x == 0 == y:
                        if self.grid[y + 1][x] != 0 or self.grid[y][x + 1] != 0 \
                                or self.grid[y + 1][x + 1] != 0:
                            moves.append([y, x])
                            continue
                    if x == 14 == y:
                        if self.grid[y - 1][x] != 0 or self.grid[y][x - 1] != 0 \
                                or self.grid[y - 1][x - 1] != 0:
                            moves.append([y, x])
                            continue
                    if 0 < y < 14 == x:
                        if self.grid[y + 1][x] != 0 or self.grid[y - 1][x] != 0 or self.grid[y][x - 1] != 0 \
                                or self.grid[y + 1][x - 1] != 0 or self.grid[y - 1][x - 1] != 0:
                            moves.append([y, x])
                            continue
                    if 0 < x < 14 == y:
                        if self.grid[y - 1][x] != 0 or self.grid[y][x + 1] != 0 or self.grid[y][x - 1] != 0 \
                                or self.grid[y - 1][x + 1] != 0 or self.grid[y - 1][x - 1] != 0:
                            moves.append([y, x])
                            continue
                    if x == 0 and y == 14:
                        if self.grid[y - 1][x] != 0 or self.grid[y][x + 1] != 0 \
                                or self.grid[y - 1][x + 1] != 0:
                            moves.append([y, x])
                            continue
                    if x == 14 and y == 0:
                        if self.grid[y + 1][x] != 0 or self.grid[y][x - 1] != 0 \
                                or self.grid[y + 1][x - 1] != 0:
                            moves.append([y, x])
                            continue
        return moves
