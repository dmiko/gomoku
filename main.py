import pygame
import random
from board import Board
from ai import Ai
from settings import AI_PLAYER
from settings import HUMAN_PLAYER
from settings import WIN_SCORE

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BACKGROUND_COLOR = (239, 206, 115)
SCREEN_SIZE = 750

pygame.init()
pygame.display.set_caption("Gomoku")
screen = pygame.display.set_mode((SCREEN_SIZE, SCREEN_SIZE))

board = Board()
ai = Ai(board)

actual_player = random.randint(HUMAN_PLAYER, AI_PLAYER)
ai_move = False
if actual_player == AI_PLAYER:
    ai_move = True


def draw_circle(color, position):
    pygame.draw.circle(screen, color, position, 20)


def print_winner_text(player):
    font = pygame.font.Font('freesansbold.ttf', 32)
    if player == HUMAN_PLAYER:
        text = font.render('Black wins!', True, BLACK, WHITE)
    else:
        text = font.render('White wins!', True, WHITE, BLACK)
    text_rect = text.get_rect()
    text_rect.center = (SCREEN_SIZE // 2, SCREEN_SIZE // 2)
    screen.blit(text, text_rect)


running = True
while running:
    for event in pygame.event.get():

        if event.type == pygame.QUIT:
            running = False

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                board.reset_grid()
                pygame.event.set_allowed(pygame.MOUSEBUTTONDOWN)
                actual_player = random.randint(HUMAN_PLAYER, AI_PLAYER)
                ai_move = False
                if actual_player == AI_PLAYER:
                    ai_move = True

        if actual_player == HUMAN_PLAYER:
            if event.type == pygame.MOUSEBUTTONDOWN:
                if pygame.mouse.get_pressed()[0]:
                    pos = pygame.mouse.get_pos()
                    pos_x = pos[0] // 50
                    pos_y = pos[1] // 50
                    if board.set_cell_value(pos_y, pos_x, actual_player):
                        ai_move = True

    if actual_player == AI_PLAYER and ai_move:
        ai.do_move()

    screen.fill(BACKGROUND_COLOR)
    board.draw(screen, SCREEN_SIZE, BLACK)

    for x in range(15):
        for y in range(15):
            if board.grid[y][x] == HUMAN_PLAYER:
                draw_circle(BLACK, (((x + 1) * 50) - 25, ((y + 1) * 50) - 25))
            if board.grid[y][x] == AI_PLAYER:
                draw_circle(WHITE, (((x + 1) * 50) - 25, ((y + 1) * 50) - 25))

    if board.get_score(actual_player) >= WIN_SCORE:
        ai_move = False
        pygame.event.set_blocked(pygame.MOUSEBUTTONDOWN)
        print_winner_text(actual_player)
    else:
        if actual_player == HUMAN_PLAYER and ai_move:
            actual_player = AI_PLAYER
        else:
            ai_move = False
            actual_player = HUMAN_PLAYER

    pygame.display.flip()
